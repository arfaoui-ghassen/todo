## About TODO 

build with 
- laravel 7.x
<!-- - MaDbodb  -->
- Mongo Db
- Reactjs
- docker

## Installation


1- git clone https://gitlab.com/arfaoui-ghassen/todo

2- if you want to use docker please run :
		
		docker-compose build	
		docker-compose up
3- rename .env-example to .env 

4- run the artisan command 
	
	php artisan todo:install  ==> install and set config
	php artisan todo:users ==> to get some test users
6- start your php server 
	
	php artisan serve

7- cd to reactjs_app

6-run command 
	npm install
	npm start

8- enjoy the todo app and play with the code if y want to 

## For any question arf.ghassen@outlook.fr