import React, {useState, useEffect} from "react";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTrash,faCheck,faSignOutAlt } from '@fortawesome/free-solid-svg-icons'
import Cookies from 'universal-cookie';
//css
import "./style.css"
//importing form componenets
import TodoForm from "./TodoForm";
import TodoServices from "../../services/TodoServices";


function Todo() {
    const cookie=new Cookies();
    const services = new TodoServices();
    const [todos, setTodos] = useState([]);
    const [reload, setReload] = useState(false);
    const  logout = async(event)=> {
      event.preventDefault();
      cookie.remove("access_token");
      setReload()
    }

    if ( ! cookie.get('access_token')){
        window.location.href = "/";
    }
      useEffect(() => {

          services.getUserTasks().then(res => {
              setTodos(res)
          })

  }, []);

            useEffect(() => {

          const services = new TodoServices();
          services.getUserTasks().then(res => {
              setTodos(res)
          })

  }, [reload]);

    const  removeTodo = async(event, id)=> {
      event.preventDefault();
      services.deleteTask(id).then(res => {
              if(res){
                  setReload()
              }
          })
    }
    const  toggleComplete = async(event, id)=> {
      event.preventDefault();
      services.completeTask(id).then(res => {
              if(res){
                  setReload()
              }
          })
    }
    return (
        <div>
            <a href="/" className="logout" onClick={(e) => logout(e)}> <FontAwesomeIcon icon={faSignOutAlt}  /></a>
            <TodoForm callBack={setReload} />
            <ul>
                {todos.map((todo,key)=> (
                    <li key={key}>
                        <div className="row">
                            <div className="col-md-8">
                                {todo.complete === 1 ? <del>{todo.name}</del> : todo.name }
                            </div>
                            <div className="col-md-4">
                                <a href='/' onClick={(e) => toggleComplete(e,todo._id)}> <span className="text-success"> <FontAwesomeIcon icon={faCheck}  /></span> </a>
                                <a href='/' onClick={(e) => removeTodo(e,todo._id)}> <span className="text-danger"><FontAwesomeIcon icon={faTrash}  /></span> </a>
                            </div>
                        </div>


                    </li>
              ))}
            </ul>
        </div>
    );
}
export default Todo;