import React,{ useState } from 'react';
import TodoServices from "../../services/TodoServices";
import './style.css'
const _servi =new TodoServices();

function TodoForm(props){
    const [todo,setTodo]=useState({
        id:"",
        task:"",
        completed: false

    });


function handleTaskInputChanges(e){
    setTodo({...todo, task: e.target.value});
}

function handleSubmit(e){
    e.preventDefault();
    if(todo.task.trim()){
        _servi.addTask(todo.task.trim()).then(res => {
            //window.location.reload();
            props.callBack(true);
        }).catch(err => {
            console.log(err);
             alert("error");
        })



        }
    }
    return(
            <div>
                <form className="form-inline" onSubmit={handleSubmit}>
                    <input
                        className="form-control"
                        name="task"
                        type="text"
                        value={todo.task}
                        onChange={handleTaskInputChanges}
                        placeholder="Enter your next TASK"
                    />
                    <div className="col-auto my-1">
                        <button  className="btn btn-warning" type="submit">Add</button>
                    </div>
                </form>
            </div>
    );
}
export default TodoForm


