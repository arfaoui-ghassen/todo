import React, {useState} from "react";
import AuthService from './../../services/AuthService';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {useHistory} from 'react-router-dom';
import { faUser,faLock } from '@fortawesome/free-solid-svg-icons'


const Login =()=>  {

  const [username ,setUsername]= useState('')
  const [password ,setPassword]= useState('')
  let history = useHistory();


  const  handleFormSubmit = async(event)=> {
    event.preventDefault();
    const postData = {username,password}
     const response = await AuthService.doUserLogin(postData);
     if(response){
        const login_handler=AuthService.handleLoginSuccess(response);
        if(login_handler){
          history.push("/todo");
        }

     }else{
       alert("please check your credentials");
     }
  }
    return (
      <React.Fragment>
        <div className="login-page">
          <div className="login-box">
            <div className="card">
              <div className="card-body login-card-body">
                <p className="login-box-msg">Sign in to start your session</p>

                <form onSubmit={handleFormSubmit}>
                  <div className="input-group mb-3">
                    <input type="email"
                      name="name"
                      className="form-control"
                      placeholder="Email"
                      value={username}
                      onChange={event => setUsername(event.target.value)} />
                    <div className="input-group-appdoUserLogin end">
                      <div className="input-group-text">
                        <span><FontAwesomeIcon icon={faUser}  /></span>
                      </div>
                    </div>
                  </div>
                  <div className="input-group mb-3">
                    <input type="password"
                      name="password"
                      className="form-control"
                      placeholder="Password"
                      value={password}
                      onChange={event => setPassword(event.target.value)}  />
                    <div className="input-group-append">
                      <div className="input-group-text">
                        <span><FontAwesomeIcon icon={faLock}  /></span>
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-6">
                      <button type="submit" className="btn btn-primary btn-block">Sign In</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
}

export default Login;