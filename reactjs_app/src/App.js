import React ,{ useState, useEffect } from "react";
import {BrowserRouter as Router , Route ,Redirect} from "react-router-dom";
import TodoList from "./components/todo/TodoList";
import Login from "./components/login/Login"
import Todo from "./components/todo/Todo";
import Cookies from 'universal-cookie';

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faHeart } from '@fortawesome/free-solid-svg-icons'
import './App.css';

const cookie=new Cookies();


const LOCCAL_STORAGE_KEY="todo-list-todos";

function App() {

const protectedRoutes: Array<any> = [
  { path: "/todo", component: TodoList, exact: true },
];

  return (
        <div className="App">
          <header className="App-header">
              <p>TODO</p>
              <Router>
                  {/*<Redirect strict from="/" to="/login" />*/}
                  <Route path='/' exact>
                        <Login/>
                  </Route>
                  <Route path='/todo' exact >
                        <Todo/>
                  </Route>
              </Router>
          </header>
            <footer>
                Made with <span className="text-danger"> <FontAwesomeIcon icon={faHeart}  /></span> by <a  href="https://www.linkedin.com/in/arf-ghassen/">Ghassen</a>
            </footer>
        </div>
  );
}

export default App;
