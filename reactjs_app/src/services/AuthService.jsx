
import UrlServices from "./UrlServices";
import Cookies from 'universal-cookie';

import axios from 'axios';

const cookie=new Cookies();
const expireAt=60*24;
class AuthService {
    /*
    * params credentials (username/password)
    * */
    async doUserLogin(credentials: Credentials){
        try {
            const respose=await axios.post(UrlServices.LoginUrl(),credentials);
            return respose.data;
        }catch (error){
            console.log(error.response);
            return false;
        }
    }

    handleLoginSuccess(response:any){
        // setting cookie expiration date
        let date = new Date();
        date.setTime(date.getTime()+(expireAt*60*1000));
        const option ={ path:'/' ,expires:expireAt};
        console.log("setting cookies ")
        cookie.set('access_token',response.access_token, {option});


        return true;
    }
}




export default new AuthService();