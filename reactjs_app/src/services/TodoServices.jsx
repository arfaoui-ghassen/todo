
import Cookies from 'universal-cookie';
import axios from 'axios';
import UrlServices from "./UrlServices";
class TodoServices{
     //get user id from the cookie
    cookie=new Cookies();
    token=this.cookie.get('access_token');

    //request access token params
    config = {
            headers: { Authorization: `Bearer ${this.token}` }
        };
    /*
    * send Store request
    * */
    async addTask(task: string){
        //prepare object to send
        const postData = {
          task : task
        }
        try {
             const response=await axios.post(UrlServices.todoUrls('save'),postData,this.config);
             if(response){
                 return true;
             }
        }catch (error){
            return false;
        }
    }
     async getUserTasks(){
        try {
            const respose=await axios.get(UrlServices.todoUrls('getTasks'),this.config)
            return respose.data;

        }catch (error){
           alert("error");
           console.log(error);
        }

    }
    async deleteTask(id){
        try {

             const respose=await axios.get(UrlServices.todoUrls('delete')+'/'+id,this.config)
             return respose.data;

        }catch (error){
           alert("error");
           console.log(error);
        }

    }
    async completeTask(id){
        try {

             const respose=await axios.get(UrlServices.todoUrls('complete')+'/'+id,this.config)
             return respose.data;

        }catch (error){
           alert("error");
           console.log(error);
        }

    }


}
export default TodoServices;