

let apiDomain="";

if (process.env.NODE_ENV=== 'production'){
    apiDomain='yourWebsiteUrl'
}else{
    apiDomain='http://localhost:8000/'
}
class UrlServices{

static LoginUrl(){

    return apiDomain+'api/login';
}

static todoUrls(operation){
    var x= {
        getTasks: "/",
        save: "create",
        delete:"delete",
        complete:"complete"
    };
    return apiDomain+'api/todo/'+x[operation];
}



}
export default UrlServices;