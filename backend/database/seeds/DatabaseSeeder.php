<?php

use Illuminate\Database\Seeder;
use App\User;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        factory(User::class,20)->create();
        User::create([
        	'name'=>'test_user',
        	'email'=>'admin@admin.com',
        	'password'=>bcrypt('admin'),


        ]);
    }
}
