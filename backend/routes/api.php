<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Laravel\Passport\Http\Controllers\AccessTokenController;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('login',[AccessTokenController::class,'issueToken'])
->middleware(['api-login','throttle']);


Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});



Route::group(['prefix' => 'todo',  'middleware' => 'auth:api'], function()
{
     Route::get('/',"TodoController@index");
     Route::post('create',"TodoController@store");
     Route::get('delete/{id}',"TodoController@destroy");
     Route::get('complete/{id}',"TodoController@complete");
});




Route::get('/testHashpass',function(){
	$mdp='admin';
	$hashed_mdp=bcrypt($mdp);
	var_dump($hashed_mdp);
	$cr=Hash::check($mdp,$hashed_mdp);
	return json_encode($cr);


});