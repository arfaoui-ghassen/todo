<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model;
class Todo extends Model
{
    /*
     * Table name
     */
    protected $table = 'todos';

    /*
     * Fillable fields for protecting mass assignment vulnerability
     */
    protected $fillable = [
        'name',
        'user_id',
        'complete'
        
    ];
}