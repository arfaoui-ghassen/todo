<?php

namespace App\Http\Middleware;

use Closure;

class ApiLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $secret=Config('app.apiToken');
        $client_id=Config('app.clientId');
        $request->merge([
            'grant_type'=>'password',
            'client_id'=>$client_id,   
            'client_secret'=>$secret,
        ]);
        return $next($request);
    }
}
