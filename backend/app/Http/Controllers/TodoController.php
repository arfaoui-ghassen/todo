<?php

namespace App\Http\Controllers;

use App\Todo;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TodoController extends Controller
{
    /**
     * View ToDos listing.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $todoList = Todo::where('user_id', Auth::id())->get();
        return json_encode($todoList,true);
    }

    /**
     * Create new Todo.
     *
     * @param Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
    	//request validation 
		  $validator = \Validator::make(
		  	['task' => $request->task], 
		  	['task' => 'required|max:255']
		  );
        if ($validator->fails())
        	return json_encode($validator->errors,true);
        try {
	        Todo::create([
	            'name' => $request->get('task'),
	            'user_id' => Auth::user()->id,
        		'complete'=>0

	        ]);	
	        return true;
        } catch (Exception $e) {
	        return json_encode([
	        	'error'=>true,
	        	'message'=> $e->errors()
	    	]);
        }
    }

    /**
     * Toggle Status.
     *
     * @param $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id)
    {
        $todo = Todo::findOrFail($id);
        $todo->complete = !$todo->complete;
        $todo->save();
	    return json_encode(true);
        
    }

    /**
     * Delete Todo.
     *
     * @param $id
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy($id)
    {
    	//get todo 
        $todo = Todo::find($id);
        //verify that the todo exist
        if(!isset($todo) && empty($todo))
        	return json_encode(array(
        		'error'=>true,
        		'message'=>"this todo may be already deleted"
        		));
    	//verify that this todo belong to Auth
        if($todo->user_id!=Auth::id())
        	return json_encode(array(
        		'error'=>true,
        		'message'=>"this todo doesn't belong to this user"
        		));	
        $todo->delete();
        return json_encode(true);
        
    }
    public function complete($id){
                //get todo 
        $todo = Todo::find($id);
        //verify that the todo exist
        if(!isset($todo) && empty($todo))
            return json_encode(array(
                'error'=>true,
                'message'=>"this todo may be already deleted"
                ));
        //verify that this todo belong to Auth
        if($todo->user_id!=Auth::id())
            return json_encode(array(
                'error'=>true,
                'message'=>"this todo doesn't belong to this user"
                )); 
        $todo->complete=1;
        $todo->save();
        return json_encode(true);

    }
}