<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
class TodoInstall extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'todo:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Install the project';

    /**
     * Create a new command instance.
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

    $this->execShellWithPrettyPrint('php artisan key:generate');
    $this->execShellWithPrettyPrint('php artisan migrate:fresh ');
    $this->execShellWithPrettyPrint('php artisan passport:install --force');
    $this->execShellWithPrettyPrint('php artisan db:seed');
    $conf=DB::table("oauth_clients")->where("name",'Laravel Password Grant Client')->first();
    config(['app.clientId' => get_object_vars ($conf['_id'])["oid"]]);
    config(['app.apiToken' => $conf['secret']]);
    $fp = fopen(base_path() .'/config/app.php' , 'w');
    fwrite($fp, '<?php return ' . var_export(config('app'), true) . ';');
    fclose($fp);
    $this->info('your secret KEY has been saved under config/app.php ');
    $this->info('your secret KEY : '.$conf['secret']);
    $this->execShellWithPrettyPrint('php artisan todo:users');
    }
    /**
     * Exec shell with pretty print.
     *
     * @param  string $command
     * @return mixed
     */
    public function execShellWithPrettyPrint($command)
    {
        $this->info('---');
        $this->info($command);
        $output = shell_exec($command);
        $this->info($output);
        $this->info('---');
    }
}
